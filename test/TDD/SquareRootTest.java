package TDD;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.*;

public class SquareRootTest {

	@Before
	public void beforeTest() {
		System.out.println("=========================");
	}

	@After
	public void afterTest() {
		System.out.println("=========================\n\n");
	}

	@Test
	public void testSquareRootBetween4and40() throws Exception {
		// entre 4 et 40
		System.out.println("entre 4 et 40\n");
		int t1[] = {2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4};
		int t2[] = Methods.squareRootBetween(4, 16);
		System.out.println("t1: " + Arrays.toString(t1));
		System.out.println("t2: " + Arrays.toString(t2));
		assertArrayEquals(t1, t2);
	}

	@Test(expected = Exception.class)
	public void testSquareRootBetweenM4and4() throws Exception {
		System.out.println("entre -4 et 4\n");
		Methods.squareRootBetween(-4, 4);
	}

}
