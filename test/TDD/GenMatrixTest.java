package TDD;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class GenMatrixTest {

	@Before
	public void beforeTest() {
		System.out.println("=========================");
	}

	@After
	public void afterTest() {
		System.out.println("=========================\n\n");
	}

	@Test
	public void testGenMatrix() throws Exception {
		int m1[][] = Methods.genMatrix(100, 100, 0, 10);
		int m2[][] = Methods.genMatrix(100, 100, 0, 10);
		boolean equals = true;
		for (int i  = 0; i < 100; i++) {
			for (int j = 0; j < 100; j++) {
				if (m1[i][j] != m2[i][j]) {
					equals = false;
					break;
				}
			}
		}
		assertNotEquals(true, equals);
	}

	@Test(expected = Exception.class)
	public void testGenMatrixNegative() throws Exception {
		Methods.genMatrix(-4, -4, 0, 100);
	}

	@Test(timeout = 50)
	public void testGenMatrixTime() throws Exception {
		Methods.genMatrix(1000, 1000, 0, 100);
	}

}
