package TDD;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.*;

public class PalindromesTest {

	@Before
	public void beforeTest() {
		System.out.println("=========================");
	}

	@After
	public void afterTest() {
		System.out.println("=========================\n\n");
	}

	@Test
	public void testPalindromes() throws Exception {
		System.out.println("1 palindrome");
		assertEquals(1, Methods.palindromes("kayak"));
	}

	@Test
	public void testPalindromes2() throws Exception {
		System.out.println("2 palindromes");
		assertEquals(2, Methods.palindromes("kayak lol"));
	}

	@Test
	public void testPalindromePair() throws Exception {
		System.out.println("1 palindrome");
		assertEquals(1, Methods.palindromes("abba"));
	}

	@Test
	public void testPalindromeAucun() throws Exception {
		System.out.println("0 palindrome");
		assertEquals(0, Methods.palindromes("abab"));
		assertEquals(0, Methods.palindromes("baba"));
		assertEquals(0, Methods.palindromes("ababb"));
	}

	@Test(expected = Exception.class)
	public void testPalindromeRien() throws Exception {
		System.out.println("exception!");
		Methods.palindromes("");
	}

}
