package TDD;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class TriangleTest {

	@Before
	public void beforeTest() {
		System.out.println("=========================");
	}

	@After
	public void afterTest() {
		System.out.println("=========================\n\n");
	}

	@Test(expected = Exception.class)
	public void testTriangleErreur() throws Exception {
		System.out.println("exception: -1 -1 -1\n");
		Methods.triangle(-1, -1, -1);
	}

	@Test
	public void testTriangleNon() throws Exception {
		System.out.println("non: 0 3 4\n");
		assertEquals(4, Methods.triangle(0, 3, 4));
	}

	@Test
	public void testTriangleNon2() throws Exception {
		System.out.println("non2: 1 1 3\n");
		assertEquals(4, Methods.triangle(1, 1, 3));
	}

	@Test
	public void testTriangleScalene() throws Exception {
		System.out.println("scalène: 1 2 3\n");
		assertEquals(3, Methods.triangle(1, 2, 3));
	}

	@Test
	public void testTriangleIsocele() throws Exception {
		System.out.println("isocèle: 1 2 2\n");
		assertEquals(1, Methods.triangle(1, 2, 2));
	}

	@Test
	public void testTriangleIsoceleNon() throws Exception {
		System.out.println("non3: 2 2 5\n");
		assertEquals(4, Methods.triangle(2, 2, 5));
	}

	@Test
	public void testTriangleEquilateral() throws Exception {
		System.out.println("equilatéral: 3 3 3\n");
		assertEquals(2, Methods.triangle(3, 3, 3));
	}

}
