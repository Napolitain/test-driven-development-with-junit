package TDD;

import static java.lang.Math.*;

/**
 * Tous les objets.
 */
public class FieldObject {

    /**
     * Les objets sont caractérisés par un poids et des coordonnées.
     * Ils appartiennent à un terrain, et peuvent être portés ou non.
     */
    protected Field field;
    protected int weight;
    protected double x, y;
    protected boolean lifted;

    /**
     * Constructeur.
     *
     * @param f  Terrain sur lequel vit l'objet.
     * @param w  Poids
     * @param x  Abscisse
     * @param y  Ordonnée
     */
    public FieldObject(Field f, int w, double x, double y) {
		// À compléter.
		field = f;
		weight = w >= 1 ? w : 1;
		if (x < 0) {
			this.x = 0;
		} else if (x > 10) {
			this.x = 10;
		} else {
			this.x = x;
		}
		if (y < 0) {
			this.y = 0;
		} else if (y > 8) {
			this.y = 8;
		} else {
			this.y = y;
		}
    }

    /**
     * Renvoie le poids de l'objet.
     *
     * @return Poids
     */
    public int getWeight() {
		// À compléter.
		return weight;
    }

    /**
     * Définit la position de l'objet.
     *
     * @param x  Abscisse
     * @param y  Ordonnée
     */
    public void unsafeSetPosition(double x, double y) {
		// À compléter.
		this.x = x;
		this.y = y;
    }

}
