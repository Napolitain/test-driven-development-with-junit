package TDD;

import static java.lang.Math.*;

/**
 * Classe pour le terrain où évolueront les objets.
 */

public class Field {

    /**
     * Hauteur et largeur du terrain.
     */
    protected int heigth, width;

    /**
     * Construction d'un terrain de dimensions données.
     *
     * @param h  Hauteur du terrain
     * @param w  Largeur du terrain
     */
    public Field(int h, int w) {
		// À compléter.
		heigth = h >= 1 ? h : 1;
		width = w >= 1 ? w : 1;
    }

    /**
     * Normalisation d'une abscisse.
     *
     * @param x  Abscisse quelconque
     * @return Abscisse normalisée
     */
    public double normalizeX(double x) {
		// À compléter.
		if (x < 0) {
			return 0;
		} else if (x > 10) {
			return 10;
		} else {
			return x;
		}
    }

    /**
     * Normalisation d'une ordonnée.
     *
     * @param y  Ordonnée quelconque
     * @return Ordonnée normalisée
     */
    public double normalizeY(double y) {
		// À compléter.
		if (y < 0) {
			return 0;
		} else if (y > 8) {
			return 8;
		} else {
			return (double) (int) y;
		}
    }

}
