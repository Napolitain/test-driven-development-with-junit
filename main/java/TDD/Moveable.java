package TDD;

import static java.lang.Math.*;

/**
 * Les objets mobiles.
 */
public class Moveable extends FieldObject {

    /**
     * Niveau de carburant.
     */
    protected double fuel;

    /**
     * Constructeur.
     *
     * @param f  Terrain sur lequel vit l'objet.
     * @param w  Poids
     * @param x  Abscisse
     * @param y  Ordonnée
     */
    public Moveable(Field f, int w, double x, double y) {
		super(f, w, x, y);
		// À compléter.
		fuel = 10;
    }

    /**
     * Calcul de la distance à un point de coordonnées données.
     *
     * @param x  Abscisse
     * @param y  Ordonnée
     * @return Distance
     */
    protected double dist(double x, double y) {
		// À compléter.
		return Math.sqrt((x-this.x)*(x-this.x) + (y-this.y)*(y-this.y));
    }

    /**
     * Se déplacer vers un point de coordonnées données.
     *
     * @param x  Abscisse cible
     * @param y  Ordonnée cible
     */
    public void goTo(double x, double y) {
		// À compléter.
		double nx = field.normalizeX(x);
		double ny = field.normalizeY(y);
		if (fuel >= dist(nx, ny)) {
			fuel -= dist(nx, ny);
			this.x = nx;
			this.y = ny;
		} else {
			double dx = abs(nx - this.x);
			double dy = abs(ny - this.y);
			double ratio = dist(nx, ny) / fuel;
			this.x += nx >= this.x ? dx/ratio : -dx/ratio;
			this.y += ny >= this.y ? dy/ratio : -dy/ratio;
			fuel = 0;
		}
	}

}
