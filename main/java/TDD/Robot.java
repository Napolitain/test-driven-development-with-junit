package TDD;

import static java.lang.Math.*;
import java.util.ArrayList;

/**
 * Les robots.
 */
public class Robot extends Moveable {

    /**
     * Charge maximale et objets transportés.
     */
    protected int maxLoad;
    protected ArrayList<FieldObject> cargo;

    /**
     * Constructeur.
     *
     * @param f  Terrain sur lequel vit l'objet.
     * @param w  Poids
     * @param x  Abscisse
     * @param y  Ordonnée
     * @param l  Charge maximale
     */
    public Robot(Field f, int w, double x, double y, int l) {
	    super(f, w, x, y);
	    // À compléter.
		maxLoad = l < 0 ? 0 : l;
		cargo = new ArrayList<FieldObject>();
    }
    
    /**
     * Calcul de la charge actuelle.
     *
     * @return Charge actuelle
     */
    public int cargoWeight() {
        // À compléter.
		int actualWeight = 0;
		if (!cargo.isEmpty()) {
			for (FieldObject fo: cargo) {
				actualWeight += fo.getWeight();
			}
			return actualWeight;
		} else {
			return 0;
		}
    }

    /**
     * Prendre un objet.
     *
     * @param o  Objet à prendre
     */
    public void lift(FieldObject o) {
	    // À compléter.
		if (dist(o.x, o.y) <= 1) {
			weight += o.weight;
			cargo.add(o);
			o.lifted = true;
		}
	}

    /**
     * Déposer un objet.
     *
     * @param o  Objet à déposer
     */
    public void dropOff(FieldObject o) {
	    // À compléter.
		if (cargo.contains(o) && lifted != true) {
			cargo.remove(o);
			weight -= o.weight;
			o.unsafeSetPosition(x, y);
			o.lifted = false;
		}
    }

	@Override
	public void goTo(double x, double y) {
    	if (cargoWeight() <= maxLoad && lifted != true) {
    		super.goTo(x, y);
		}
	}
}
