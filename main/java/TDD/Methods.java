package TDD;

import java.util.Random;

public class Methods {

	public static int[] squareRootBetween(int a, int b) throws Exception {
		if (a <= -1) {
			throw new Exception("Les valeurs négatives sont interdites");
		}
		int tab[] = new int[b-a+1];
		int indice = 0;
		for(int i = a; i <= b; i++) {
			double sr = Math.sqrt(i);
			tab[indice] = (int) sr;
			indice++;
		}
		return tab;
	}

	public static int[][] genMatrix(int m, int n, int a, int b) throws Exception {
		if (m <= 0 || n <= 0) {
			throw new Exception("m et n doivent être >= 1");
		}
		int matrix[][] = new int[m][n];
		Random r = new Random();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				matrix[i][j] = r.nextInt(b-a) + a;
			}
		}
		return matrix;
	}

	public static int palindromes(String phrase) throws Exception {
		if (phrase.isEmpty()) {
			throw new Exception("Phrase vide");
		}
		String[] mots = phrase.split(" ");
		int compteur = 0;
		for (String word : mots) {
			boolean isPalindrome = true;
			int  lengthOfCurrentWord = word.length();
			for (int i = 0; i <= lengthOfCurrentWord / 2; i++) {
				if (word.charAt(i) != word.charAt(lengthOfCurrentWord-1-i)) {
					isPalindrome = false;
				}
			}
			if (isPalindrome) {
				compteur++;
			}
		}
		return compteur;
	}

	public static int triangle(int a, int b, int c) throws Exception {
		int max = Math.max(Math.max(a, b), c);
		if (a < 0 || b < 0 || c < 0) {
			throw new Exception("a, b ou c doivent être >= 0 pour faire de la géométrie");
		}
		// pas un triangle si un coté est nul ou le coté maximale
		// est plus grand que la somme des deux autres
		if (a == 0 || b == 0 || c == 0 || 2*max > a+b+c) {
			return 4;
		} else if (a == b && b == c && a == c) { // équilatéral
			return 2;
		} else if (a == b || b == c || a == c) { // isocèle
			return 1;
		} else { // triangle scalène
			return 3;
		}
	}

}
